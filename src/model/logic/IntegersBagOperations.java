package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
		}
		return max;
	}

	public int getMin(IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if(min > value)
				{
					min = value;
				}
			}
		}
		return min;
	}

	public String getEvenNumbers(IntegersBag bag)
	{
		int value;
		String rta = "";
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if(value%2 == 0)
				{
					rta += ", " + value;
				}
			}
		}
		return rta;
	}

	public String getPrimeNumbers(IntegersBag bag)
	{
		int value;
		String rta = "";
		boolean prime = true;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				int counter = 0;
				for(int i = 1; i <= value; i++)
				{	
					if(value % i == 0)
					{
						counter = counter + 1;
					}
				}
				if(counter == 2)
				{
					rta += ", " + value;
				}
			}
		}
		return rta;
	}
}
